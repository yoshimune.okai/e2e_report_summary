# E2Eテストframeworkの調査と実装について
#### E2Eテストの要件
- クロスブラウザ検証
- ランタイムの計測
- スクリーンショットの取得/デモ動画の取得
- 仮想環境(docker)での動作
- gitlabCIでの動作
- テストレポートの管理・可視化


#### 調査対象
WebフロントエンドのE2Eテストを行うためのframework
- Playwright
- Cypress
- testCafe

#### framework共通
- Node.jsのライブラリとして提供  
- ヘッドレスモードが可能  
- 画面録画，スナップショットによる記録が可能  
- ランタイム計測が可能
- インストールが容易(npm)  


#### frameworkの比較
○：良い  
△：どちらとも言えない  
×：悪い  

|項目|Playwright |Cypress  |TestCafe |
|----|----|----|----|
|クロスブラウザ|○|×|○|
|拡張性 |○|△|△|
|導入しやすさ  |○|○|○|
|書きやすさ  |△|○○|○|

- [npmトレンド](https://www.npmtrends.com/cypress-vs-playwright-vs-selenium-webdriver-vs-testcafe)



※ いずれもDockerで公式イメージが用意されている

--------

### 1. [Playwright](https://playwright.dev/)
##### 特徴
- クロスブラウザ(Chrome,Safari,Firefox)に対応    
- [jest(テストフレームワーク)](https://jestjs.io/)と組み合わせることにより，ユニットテストとして実装が可能  
 

##### テストコード
- スナップショットを撮る<br>
(https://gitlab.com/yoshimune.okai/e2e_framework/Playwright)

> 備考
> Microsoft社製のブラウザ自動化ライブラリ  
> 元々GoogleでPuppeteerを開発していたチームがMicrosoftに移り，開発した．  
> そのため，大まかな部分はPuppeteerをベースにしている．  

------

### 2. [Cypress](https://www.cypress.io/)

##### 長所
- async/awaitを記述せずに，同期処理が書ける  
- 検証項目毎にコード分割(ユニットテスト)ができる    
- デフォルト機能が豊富(スナップショットの取得，ランタイム計測，エラー記録等)なため，初期設定がほとんど不要  


##### 短所
- クロスブラウザ未対応

> 現在(2020年10月時点)，chromiumやfirefoxには対応しているが，safari(webkit)に対応していない．  ただし，最近は対応ブラウザも増えてきているので，今後safariも対応する可能性あり．  


##### テストコード
Googleの検索で，目的のサイトが出現するか  
(https://gitlab.com/yoshimune.okai/e2e_framework/-/tree/master/Cypress)

------

### 3. [TestCafe](https://devexpress.github.io/testcafe/)

##### 長所
async/awaitを記述せずに，同期処理が書ける  
クロスブラウザ(Chrome,Safari,Firefox等)に対応    


##### 短所
JavaScript,typeScriptのみサポート  

##### テストコード
https://gitlab.com/yoshimune.okai/e2e_framework/-/tree/master/testCafe



### 直近の実装
[codeceptJS](https://codecept.io/)(wrapperの一種)を利用したE2Eテストランナーの実装

##### codeceptJSの利点
- 導入・メンテナンスが容易  
- framework(Playwright,Cypress,TestCafe)  では対応していない機能もプラグインとして追加できる  

##### 機能例
- スクリーンショットの撮影(成功時or失敗時の設定可能)  
- キャッシュ機能あり  
- テストのリトライ機能あり  
- データ駆動テストのサポート  
- 並列実行のサポート  
- Allure(レポーティング機能)のサポート  





